<?
        /**
        *example result query DB
        **/
        $a=array(            
            "data"=>array(
                array(
                    "name"=>"Trand",
                    "value"=>"1.1.1"
                ),
                array(
                    "name"=>"Classic",
                    "value"=>"1.1.2"
                ),
                array(
                    "name"=>"BTrand",
                    "value"=>"1.1.3"
                ),
                array(
                    "name"=>"BClassic",
                    "value"=>"1.1.4"
                )
            ),
            "default"=>"1.1.4"
        );
?>
<!doctype html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Calculate</title>
	<link rel="stylesheet" href="/scr/css/normalize.css" type="text/css">
	<link rel="stylesheet" href="/scr/css/reset.css" type="text/css">
	<script src="/scr/script/vue.js"></script>
	<style>
        .group_active_true, .group__line_active_true{
            display:block;
        }
        .group__list_active_true{
            display:inline;
        }
        .group_active_false, .group__line_active_false, .group__list_active_false{ 
            display:none
        }
    </style>
</head>
<body class="solid">
	<header class="header solid__header"></header>	
	<section class="calculated">
	    <h1 class =calculated__headline></h1>
	    <div class="parameter" id="app">
	        <div class="group-A parameter__group-A" id="group-A">	                        
                <div class="group group-A__group">
                    <span>--------------------------------------------</span>
	                <div class="group__line" id="TypeMainFrame">
                        <span class="group__title">Тип основного каркаса</span>
                        <select class="group__list" v-model="TypeMainFrame_S">
                            <option v-for="TypeMainFrame_O in TypeMainFrame_Os" v-bind:value="TypeMainFrame_O.value">
                                {{TypeMainFrame_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="TypeControl">
                        <span class="group__title">Тип управления</span>
                        <select class="group__list" v-model="TypeControl_S">
                            <option v-for="TypeControl_O in TypeControl_Os" v-bind:value="TypeControl_O.value">
                                {{TypeControl_O.name}}
                            </option>                    
                        </select>
                    </div> 
                    <div class="group__line" id="WidthOpening">
                        <span class="group__title">Ширина проема</span>
                        <input type="text" class="group__input" v-model="WidthOpening_S" v-on:keypress="noChar($event,WidthOpening_S)" v-on:keyup="messageBox('WidthOpening','keyup')" v-on:blur="messageBox('WidthOpening','blur')" data-value="1.3.0">
                        <span class="group__placeholder">{{WidthOpening_P}}</span>
                        <span class="group__warning group__warning_active_true">{{WidthOpening_M}}</span>
                    </div>
                    <div class="group__line" id="HeightOpening">
                        <span class="group__title">Высота проема</span>
                        <input type="text" class="group__input" v-model="HeightOpening_S" v-on:keypress="noChar($event,HeightOpening_S)" v-on:keyup="messageBox('HeightOpening','keyup')" v-on:blur="messageBox('HeightOpening','blur')" data-value="1.4.0">
                        <span class="group__placeholder">{{HeightOpening_P}}</span>
                        <span class="group__warning group__warning_active_true">{{HeightOpening_M}}</span>
                    </div>
                    <div class="group__line" id="DistanceOpen">
                        <span class="group__title">Расстояние для открытия ворот</span>
                        <input type="text" class="group__input" v-model="DistanceOpen_S" v-on:keypress="noChar($event,DistanceOpen_S)" v-on:keyup="messageBox('DistanceOpen','keyup')" v-on:blur="messageBox('DistanceOpen','blur')" data-value="1.3.0">
                        <span class="group__placeholder">{{DistanceOpen_P}}</span>
                        <span class="group__warning group__warning_active_true">{{DistanceOpen_M}}</span>
                    </div>
                    <div class="group__line" id="TypeRail">
                        <span class="group__title">Тип направляющей</span>
                        <select class="group__list" v-model="TypeRail_S">
                            <option v-for="TypeRail_O in TypeRail_Os" v-bind:value="TypeRail_O.value">
                                {{TypeRail_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="ColorFrame">
                        <span class="group__title">Цвет рамы</span>
                        <select class="group__list" v-model="ColorFrame_S">
                            <option v-for="ColorFrame_O in ColorFrame_Os" v-bind:value="ColorFrame_O.value">
                                {{ColorFrame_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="DirectionOpening">
                        <span class="group__title">Направление открывания (со двора)</span>
                        <select class="group__list" v-model="DirectionOpening_S">
                            <option v-for="DirectionOpening_O in DirectionOpening_Os" v-bind:value="DirectionOpening_O.value">
                                {{DirectionOpening_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="TypeFilling">
                        <span class="group__title">Тип заполнения</span>
                        <select class="group__list" v-model="TypeFilling_S">
                            <option v-for="TypeFilling_O in TypeFilling_Os" v-bind:value="TypeFilling_O.value">
                                {{TypeFilling_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="SupportPillar">
                        <span class="group__title">Поддерживающий столб</span>
                        <select class="group__list" v-model="SupportPillar_S">
                            <option v-for="SupportPillar_O in SupportPillar_Os" v-bind:value="SupportPillar_O.value">
                                {{SupportPillar_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="AperturePilar">
                        <span class="group__title">Проемный столб</span>
                        <select class="group__list" v-model="AperturePilar_S">
                            <option v-for="AperturePilar_O in AperturePilar_Os" v-bind:value="AperturePilar_O.value">
                                {{AperturePilar_O.name}}
                            </option>                    
                        </select>
                    </div>
                </div>
                <div v-bind:class="'group group-A__group group_active_' + SettingsFilling">
                    <span>--------------------------------------------</span>
                    <div class="group__line" id="MaterialSpecification">
                        <span class="group__title">Спецификация материала</span>
                        <select class="group__list" v-model="MaterialSpecification_S">
                            <option v-for="MaterialSpecification_O in MaterialSpecification_Os" v-bind:value="MaterialSpecification_O.value">
                                {{MaterialSpecification_O.name}}
                            </option>                    
                        </select>
                        <select class="group__list" v-model="MaterialSpecificationAdd_S">
                            <option v-for="MaterialSpecificationAdd_O in MaterialSpecificationAdd_Os" v-bind:value="MaterialSpecificationAdd_O.value">
                                {{MaterialSpecificationAdd_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div id="ColorFilling" v-bind:class="'group__line group__line_active_' + ColorFilling">
                        <span class="group__title">Цвет заполнения</span>
                        <select class="group__list" v-model="ColorFilling_S">
                            <option v-for="ColorFilling_O in ColorFilling_Os" v-bind:value="ColorFilling_O.value">
                                {{ColorFilling_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div v-bind:class="'group__line group__line_active_' + SpacingProfile" id="SpacingProfile">
                        <span class="group__title">Шаг между профилями</span>
                        <select class="group__list" v-model="SpacingProfile_S">
                            <option v-for="SpacingProfile_O in SpacingProfile_Os" v-bind:value="SpacingProfile_O.value">
                                {{SpacingProfile_O.name}}
                            </option>                    
                        </select>
                    </div>                
                </div>
                <div class="group group-A__group">
                    <span>--------------------------------------------</span>
                    <div class="group__line" id="RackGear">
                        <span class="group__title">Зубчатая рейка</span>
                        <select class="group__list" v-model="RackGear_S">
                            <option v-for="RackGear_O in RackGear_Os" v-bind:value="RackGear_O.value">
                                {{RackGear_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="Knob">
                        <span class="group__title">Ручка</span>
                        <select class="group__list" v-model="Knob_S">
                            <option v-for="Knob_O in Knob_Os" v-bind:value="Knob_O.value">
                                {{Knob_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="Bolt">
                        <span class="group__title">Засов с ответным кронштейном</span>
                        <select class="group__list" v-model="Bolt_S">
                            <option v-for="Bolt_O in Bolt_Os" v-bind:value="Bolt_O.value">
                                {{Bolt_O.name}}
                            </option>                    
                        </select>
                    </div>                
                </div>            
                <div v-bind:class="'group group-A__group group_active_' + Automation">
                    <span>--------------------------------------------</span>
                    <div class="group__line" id="ManufacturerAutomation">
                        <span class="group__title">Производитель автоматики</span>
                        <select class="group__list" v-model="ManufacturerAutomation_S">
                            <option v-for="ManufacturerAutomation_O in ManufacturerAutomation_Os" v-bind:value="ManufacturerAutomation_O.value">
                                {{ManufacturerAutomation_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="ModelElectricdrive">
                        <span class="group__title">Модель электропривода</span>
                        <select class="group__list" v-model="ModelElectricdrive_S">
                            <option v-for="ModelElectricdrive_O in ModelElectricdrive_Os" v-bind:value="ModelElectricdrive_O.value">
                                {{ModelElectricdrive_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="Photocells">
                        <span class="group__title">Фотоэлементы</span>
                        <select class="group__list" v-model="Photocells_S">
                            <option v-for="Photocells_O in Photocells_Os" v-bind:value="Photocells_O.value">
                                {{Photocells_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="LightWarning">
                        <span class="group__title">Сигнальная лампа</span>
                        <select class="group__list" v-model="LightWarning_S">
                            <option v-for="LightWarning_O in LightWarning_Os" v-bind:value="LightWarning_O.value">
                                {{LightWarning_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="Antenna">
                        <span class="group__title">Антенна</span>
                        <select class="group__list" v-model="Antenna_S">
                            <option v-for="Antenna_O in Antenna_Os" v-bind:value="Antenna_O.value">
                                {{Antenna_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="RemoteController">
                        <span class="group__title">Пульт ДУ</span>
                        <select class="group__list" v-model="RemoteController_S">
                            <option v-for="RemoteController_O in RemoteController_Os" v-bind:value="RemoteController_O.value">
                                {{RemoteController_O.name}}
                            </option>                    
                        </select>
                    </div>                
                </div>
                <div class="group group-A__group">
                    <span>--------------------------------------------</span>
                    <div class="group__line" id="WicketV">
                        <span class="group__title">Наличие калитки встроенной</span>
                        <select class="group__list" v-model="WicketV_S">
                            <option v-for="WicketV_O in WicketV_Os" v-bind:value="WicketV_O.value">
                                {{WicketV_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div v-bind:class="'group__line group__line_active_' + WidthWicketV" id="WidthWicketV">
                        <span class="group__title">Ширина проема</span>
                        <input type="text" class="group__input" v-model="WidthWicketV_S" v-on:keypress="noChar($event,WidthWicketV_S)" v-on:keyup="messageBox('WidthWicketV','keyup')" v-on:blur="messageBox('WidthWicketV','blur')" data-value="5.2.0">
                        <span class="group__placeholder">{{WidthWicketV_P}}</span>
                        <span class="group__warning group__warning_active_true">{{WidthWicketV_M}}</span>
                    </div>
                    <div  v-bind:class="'group__line group__line_active_' + TypeOpeningWicketV" id="TypeOpeningWicketV">
                        <span class="group__title">Тип открывания</span>
                        <select class="group__list" v-model="TypeOpeningWicketV_S">
                            <option v-for="TypeOpeningWicketV_O in TypeOpeningWicketV_Os" v-bind:value="TypeOpeningWicketV_O.value">
                                {{TypeOpeningWicketV_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div v-bind:class="'group__line group__line_active_' + DistanceWicketV" id="DistanceWicketV">
                        <span class="group__title">Расстояние от носовой части ворот</span>
                        <input type="text" class="group__input" v-model="DistanceWicketV_S" v-on:keypress="noChar($event,DistanceWicketV_S)" v-on:keyup="messageBox('DistanceWicketV','keyup')" v-on:blur="messageBox('DistanceWicketV','blur')" data-value="5.2.0">
                        <span class="group__placeholder">{{DistanceWicketV_P}}</span>
                        <span class="group__warning group__warning_active_true">{{DistanceWicketV_M}}</span>
                    </div>
                </div>
                <div class="group group-A__group">
                    <span>--------------------------------------------</span>
                    <div class="group__line" id="WicketOS">
                        <span class="group__title">Наличие отдельно стоящей калитки</span>
                        <select class="group__list" v-model="WicketOS_S">
                            <option v-for="WicketOS_O in WicketOS_Os" v-bind:value="WicketOS_O.value">
                                {{WicketOS_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div v-bind:class="'group__line group__line_active_' + WidthWicketOS" id="WidthWicketOS">
                        <span class="group__title">Ширина проема</span>
                        <input type="text" class="group__input" v-model="WidthWicketOS_S" v-on:keypress="noChar($event,WidthWicketOS_S)" v-on:keyup="messageBox('WidthWicketOS','keyup')" v-on:blur="messageBox('WidthWicketOS','blur')" data-value="6.2.0">
                        <span class="group__placeholder">{{WidthWicketOS_P}}</span>
                        <span class="group__warning group__warning_active_true">{{WidthWicketOS_M}}</span>
                    </div>
                    <div v-bind:class="'group__line group__line_active_' + HeightWicketOS" id="HeightWicketOS">
                        <span class="group__title">Высота проема</span>
                        <input type="text" class="group__input" v-model="HeightWicketOS_S" v-on:keypress="noChar($event,HeightWicketOS_S)" v-on:keyup="messageBox('HeightWicketOS','keyup')" v-on:blur="messageBox('HeightWicketOS','blur')" data-value="6.3.0">
                        <span class="group__placeholder">{{HeightWicketOS_P}}</span>
                        <span class="group__warning group__warning_active_true">{{HeightWicketOS_M}}</span>
                    </div>
                    <div v-bind:class="'group__line group__line_active_' + ColorWicketOS" id="ColorWicketOS">
                        <span class="group__title">Цвет рамы</span>
                        <select class="group__list" v-model="ColorWicketOS_S">
                            <option v-for="ColorWicketOS_O in ColorWicketOS_Os" v-bind:value="ColorWicketOS_O.value">
                                {{ColorWicketOS_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div v-bind:class="'group__line group__line_active_' + TypeOpeningWicketOS" id="TypeOpeningWicketOS">
                        <span class="group__title">Ти открывания</span>
                        <select class="group__list" v-model="TypeOpeningWicketOS_S">
                            <option v-for="TypeOpeningWicketOS_O in TypeOpeningWicketOS_Os" v-bind:value="TypeOpeningWicketOS_O.value">
                                {{TypeOpeningWicketOS_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div v-bind:class="'group__line group__line_active_' + LoopWicketOS" id="LoopWicketOS">
                        <span class="group__title">Расположение петлей (со стороны двора)</span>
                        <select class="group__list" v-model="LoopWicketOS_S">
                            <option v-for="LoopWicketOS_O in LoopWicketOS_Os" v-bind:value="LoopWicketOS_O.value">
                                {{LoopWicketOS_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div v-bind:class="'group__line group__line_active_' + TypeControlWicketOS" id="TypeControlWicketOS">
                        <span class="group__title">Тип системы доступа</span>
                        <select class="group__list" v-model="TypeControlWicketOS_S">
                            <option v-for="TypeControlWicketOS_O in TypeControlWicketOS_Os" v-bind:value="TypeControlWicketOS_O.value">
                                {{TypeControlWicketOS_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div v-bind:class="'group__line group__line_active_' + СrossbarWicketOS" id="СrossbarWicketOS">
                        <span class="group__title">Наличие перекладины</span>
                        <select class="group__list" v-model="СrossbarWicketOS_S">
                            <option v-for="СrossbarWicketOS_O in СrossbarWicketOS_Os" v-bind:value="СrossbarWicketOS_O.value">
                                {{СrossbarWicketOS_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div v-bind:class="'group__line group__line_active_' + СlearanceWicketOS" id="СlearanceWicketOS">
                        <span class="group__title">Высота просвета над створкой</span>
                        <input type="text" class="group__input" v-model="СlearanceWicketOS_S" v-on:keypress="noChar($event,СlearanceWicketOS_S)" v-on:keyup="messageBox('СlearanceWicketOS','keyup')" v-on:blur="messageBox('СlearanceWicketOS','blur')" data-value="6.8.0">
                        <span class="group__placeholder">{{СlearanceWicketOS_P}}</span>
                        <span class="group__warning group__warning_active_true">{{СlearanceWicketOS_M}}</span>
                    </div>                
                </div>
                <div class="group group-A__group">
                    <span>--------------------------------------------</span>
                    <div class="group__line" id="GateCore">
                        <span class="group__title">Закладные элементы для ворот</span>
                        <select class="group__list" v-model="GateCore_S">
                            <option v-for="GateCore_O in GateCore_Os" v-bind:value="GateCore_O.value">
                                {{GateCore_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="WicketOSCore">
                        <span class="group__title">Закладной элемент для колитки ОС</span>
                        <select class="group__list" v-model="WicketOSCore_S">
                            <option v-for="WicketOSCore_O in WicketOSCore_Os" v-bind:value="WicketOSCore_O.value">
                                {{WicketOSCore_O.name}}
                            </option>                    
                        </select>
                    </div>
                    <div class="group__line" id="WicketOSMetalPilar">
                        <span class="group__title">Металические столбы для калитки ОС</span>
                        <select class="group__list" v-model="WicketOSMetalPilar_S">
                            <option v-for="WicketOSMetalPilar_O in WicketOSMetalPilar_Os" v-bind:value="WicketOSMetalPilar_O.value">
                                {{WicketOSMetalPilar_O.name}}
                            </option>                    
                        </select>
                    </div>
                </div>
	        </div>	        
	        <div class="group-B parameter__group-B" id="group-B">	                        
                <div class="group group-B__group">
                    <span>--------------------------------------------</span>                    
                    <div class="group__line" id="GateBasis">
                        <span class="group__title">Фундаментное основание под ворота</span>
                        <select class="group__list" v-model="GateBasis_S">
                            <option v-for="GateBasis_O in GateBasis_Os" v-bind:value="GateBasis_O.value">
                                {{GateBasis_O.name}}
                            </option>                    
                        </select>
                        <select v-bind:class="'group__list group__list_active_' + GateBasisAdd_hs" v-model="GateBasisAdd_S">
                            <option v-for="GateBasisAdd_O in GateBasisAdd_Os" v-bind:value="GateBasisAdd_O.value">
                                {{GateBasisAdd_O.name}}
                            </option>                    
                        </select>
                    </div>                    
                    <div class="group__line" id="WicketOSBasis">
						<span class="group__title">Тип фундамента под калитку ОС</span>
						<select class="group__list" v-model="WicketOSBasis_S">
							<option v-for="WicketOSBasis_O in WicketOSBasis_Os" v-bind:value="WicketOSBasis_O.value">
								{{WicketOSBasis_O.name}}
							</option>                    
						</select>
					</div>                   
                    <div class="group__line" id="PowerPhotocells">
						<span class="group__title">Проводка питания под ФЭ</span>
						<select class="group__list" v-model="PowerPhotocells_S">
							<option v-for="PowerPhotocells_O in PowerPhotocells_Os" v-bind:value="PowerPhotocells_O.value">
								{{PowerPhotocells_O.name}}
							</option>                    
						</select>
					</div>                   
                    <div class="group__line" id="PowerLightWarning">
						<span class="group__title">Проводка питания под СЛ</span>
						<select class="group__list" v-model="PowerLightWarning_S">
							<option v-for="PowerLightWarning_O in PowerLightWarning_Os" v-bind:value="PowerLightWarning_O.value">
								{{PowerLightWarning_O.name}}
							</option>                    
						</select>
					</div>                    
                    <div class="group__line" id="PowerElectricdrive">
						<span class="group__title">Подводка питания для электропривода</span>
						<select class="group__list" v-model="PowerElectricdrive_S">
							<option v-for="PowerElectricdrive_O in PowerElectricdrive_Os" v-bind:value="PowerElectricdrive_O.value">
								{{PowerElectricdrive_O.name}}
							</option>                    
						</select>
					</div>                   
                    <div class="group__line" id="AboveGround">
						<span class="group__title">м.п. над землей в гофрированой трубе</span>
						<input type="text" class="group__input" v-model="AboveGround_S" v-on:keypress="noChar($event,AboveGround_S)" v-on:keyup="messageBox('AboveGround','keyup')" v-on:blur="messageBox('AboveGround','blur')" data-value="8.6.0">
						<span class="group__placeholder">{{AboveGround_P}}</span>
						<span class="group__warning group__warning_active_true">{{AboveGround_M}}</span>
					</div>                    
                    <div class="group__line" id="Underground">
						<span class="group__title">м.п. под землей в ПНД трубе</span>
						<input type="text" class="group__input" v-model="Underground_S" v-on:keypress="noChar($event,Underground_S)" v-on:keyup="messageBox('Underground','keyup')" v-on:blur="messageBox('Underground','blur')" data-value="5.2.0">
						<span class="group__placeholder">{{Underground_P}}</span>
						<span class="group__warning group__warning_active_true">{{Underground_M}}</span>
					</div>
                </div>
                <div class="group group-B__group">
                    <span>--------------------------------------------</span>
                    <div class="group__line" id="GateMounting">
						<span class="group__title">Монтаж откатных ворот </span>
						<select class="group__list" v-model="GateMounting_S">
							<option v-for="GateMounting_O in GateMounting_Os" v-bind:value="GateMounting_O.value">
								{{GateMounting_O.name}}
							</option>                    
						</select>
					</div>					
					<div class="group__line" id="WicketOSMounting">
						<span class="group__title">Монтаж отдельно стоящей калитки</span>
						<select class="group__list" v-model="WicketOSMounting_S">
							<option v-for="WicketOSMounting_O in WicketOSMounting_Os" v-bind:value="WicketOSMounting_O.value">
								{{WicketOSMounting_O.name}}
							</option>                    
						</select>
					</div>					
					<div class="group__line" id="ElectricdriveMounting">
						<span class="group__title">Монтаж электропривода </span>
						<select class="group__list" v-model="ElectricdriveMounting_S">
							<option v-for="ElectricdriveMounting_O in ElectricdriveMounting_Os" v-bind:value="ElectricdriveMounting_O.value">
								{{ElectricdriveMounting_O.name}}
							</option>                    
						</select>
					</div>					
					<div class="group__line" id="PhotocellsMounting">
						<span class="group__title">Монтаж фотоэлементов </span>
						<select class="group__list" v-model="PhotocellsMounting_S">
							<option v-for="PhotocellsMounting_O in PhotocellsMounting_Os" v-bind:value="PhotocellsMounting_O.value">
								{{PhotocellsMounting_O.name}}
							</option>                    
						</select>
					</div>					
					<div class="group__line" id="LightWarningMounting">
						<span class="group__title">Монтаж сигнальной лампы </span>
						<select class="group__list" v-model="LightWarningMounting_S">
							<option v-for="LightWarningMounting_O in LightWarningMounting_Os" v-bind:value="LightWarningMounting_O.value">
								{{LightWarningMounting_O.name}}
							</option>                    
						</select>
					</div>					
					<div class="group__line" id="FillingMounting">
						<span class="group__title">Монтаж заполнения </span>
						<select class="group__list" v-model="FillingMounting_S">
							<option v-for="FillingMounting_O in FillingMounting_Os" v-bind:value="FillingMounting_O.value">
								{{FillingMounting_O.name}}
							</option>                    
						</select>
					</div>
                </div>
                <div class="group group-B__group">
                    <span>--------------------------------------------</span>
					<div v-bind:class="'group__line group__line_active_' + MetalPilarMounting"  id="MetalPilarMounting">
						<span class="group__title">Монтаж столбов под калитку </span>
						<select class="group__list" v-model="MetalPilarMounting_S">
							<option v-for="MetalPilarMounting_O in MetalPilarMounting_Os" v-bind:value="MetalPilarMounting_O.value">
								{{MetalPilarMounting_O.name}}
							</option>                    
						</select>
					</div>					
                    <div v-bind:class="'group__line group__line_active_' + Garbage" id="Garbage">
						<span class="group__title">Вывоз строительного мусора</span>
						<select class="group__list" v-model="Garbage_S">
							<option v-for="Garbage_O in Garbage_Os" v-bind:value="Garbage_O.value">
								{{Garbage_O.name}}
							</option>                    
						</select>
					</div>					
					<div class="group__line" id="AdditionalFastening">
						<span class="group__title">Пластина  для доп. Крепления </span>
						<select class="group__list" v-model="AdditionalFastening_S">
							<option v-for="AdditionalFastening_O in AdditionalFastening_Os" v-bind:value="AdditionalFastening_O.value">
								{{AdditionalFastening_O.name}}
							</option>                    
						</select>
					</div>					
					<div class="group__line" id="RentTool">
						<span class="group__title">Аренда инструмента</span>
						<select class="group__list" v-model="RentTool_S">
							<option v-for="RentTool_O in RentTool_Os" v-bind:value="RentTool_O.value">
								{{RentTool_O.name}}
							</option>                    
						</select>
					</div>
                </div>
                <div v-bind:class="'group group-B__group group_active_' + Rent">
                    <span>--------------------------------------------</span>					
					<div class="group__line" id="Jackhammer">
						<span class="group__title">Отбойный молоток</span>
						<select class="group__list" v-model="Jackhammer_S">
							<option v-for="Jackhammer_O in Jackhammer_Os" v-bind:value="Jackhammer_O.value">
								{{Jackhammer_O.name}}
							</option>                    
						</select>
					</div>					
					<div class="group__line" id="Generator">
						<span class="group__title">Генератор</span>
						<select class="group__list" v-model="Generator_S">
							<option v-for="Generator_O in Generator_Os" v-bind:value="Generator_O.value">
								{{Generator_O.name}}
							</option>                    
						</select>
					</div>					
					<div class="group__line" id="AdditionalEquipment">
						<span class="group__title">Аренда инструмента </span>
						<select class="group__list" v-model="AdditionalEquipment_S">
							<option v-for="AdditionalEquipment_O in AdditionalEquipment_Os" v-bind:value="AdditionalEquipment_O.value">
								{{AdditionalEquipment_O.name}}
							</option>                    
						</select>
					</div>
                </div>
                <div class="group group-B__group">
                    <span>--------------------------------------------</span>                    
					<div class="group__line" id="Delivery">
						<span class="group__title">Доставка</span>
						<select class="group__list" v-model="Delivery_S">
							<option v-for="Delivery_O in Delivery_Os" v-bind:value="Delivery_O.value">
								{{Delivery_O.name}}
							</option>                    
						</select>
					</div>					
					<div v-bind:class="'group__line group__line_active_' + DeliveryDistance" id="DeliveryDistance">
						<span class="group__title">Расстояние от производства до объекта (км)</span>
						<input type="text" class="group__input" v-model="DeliveryDistance_S" v-on:keypress="noChar($event,DeliveryDistance_S)" v-on:keyup="messageBox('DeliveryDistance','keyup')" v-on:blur="messageBox('DeliveryDistance','blur')" data-value="5.2.0">
						<span class="group__placeholder">{{DeliveryDistance_P}}</span>
						<span class="group__warning group__warning_active_true">{{DeliveryDistance_M}}</span>
					</div>
					
                </div>
            </div>
	    </div>	    
	</section>
    <foter class="foter solid__foter"></foter>
	<script>
           let _vue = new Vue({
                el:"#app",
                data:{					
					DeliveryDistance_S:"",
                    DeliveryDistance_M:"___",
                    DeliveryDistance_P:"",
                    DeliveryDistance_C:{min:0, max:100000},					
					Delivery_S:"NO",
                    Delivery_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],					
					AdditionalEquipment_S:"NO",
                    AdditionalEquipment_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],					
					Generator_S:"NO",
                    Generator_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],					
					Jackhammer_S:"NO",
                    Jackhammer_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],					
					RentTool_S:"NO",
                    RentTool_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],					
					AdditionalFastening_S:"NO",
                    AdditionalFastening_Os:[{name:"Нет",value:"NO"},
                                    {name:"Пластина 5мм 150*300",value:"10.3.1"},
                                    {name:"Пластина 5мм 200*400",value:"10.3.2"}],					
					Garbage_S:"NO",
                    Garbage_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],					
					MetalPilarMounting_hs:false,
					MetalPilarMounting_S:"NO",
                    MetalPilarMounting_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],				
					FillingMounting_S:"NO",
                    FillingMounting_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],					
					LightWarningMounting_S:"NO",
                    LightWarningMounting_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],					
					PhotocellsMounting_S:"NO",
                    PhotocellsMounting_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],					
					ElectricdriveMounting_S:"NO",
                    ElectricdriveMounting_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],					
					WicketOSMounting_S:"NO",
                    WicketOSMounting_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],					
					GateMounting_S:"NO",
                    GateMounting_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],                    
					Underground_S:"",
                    Underground_M:"___",
                    Underground_P:"",
                    Underground_C:{min:0, max:100000},                    
					AboveGround_S:"",
                    AboveGround_M:"___",
                    AboveGround_P:"",
                    AboveGround_C:{min:0, max:100000},                    
					PowerElectricdrive_S:"NO",
                    PowerElectricdrive_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],                    
					PowerLightWarning_S:"NO",
                    PowerLightWarning_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],                    
					PowerPhotocells_S:"NO",
                    PowerPhotocells_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],                    
					WicketOSBasis_S:"NO",
                    WicketOSBasis_Os:[{name:"Нет",value:"NO"},
                                  {name:"Бетонное",value:"8.2.1"},
                                  {name:"Свайное",value:"8.2.2"}],                    
                    GateBasisAdd_S:"NO",                    
                    GateBasisAdd_hs:false,
                    GateBasis_S:"NO",
                    GateBasis_Os:[{name:"Нет",value:"NO"},
                                  {name:"Бетонное",value:"8.1.1"},
                                  {name:"Свайное",value:"8.1.2"}],                    
					WicketOSMetalPilar_S:"NO",
                    WicketOSMetalPilar_Os:[{name:"Нет",value:"NO"},
                                           {name:"60*60*2",value:"7.3.1"},
                                           {name:"60*80*2",value:"7.3.2"},
                                           {name:"100*100*3",value:"7.3.3"}],                    
					WicketOSCore_S:"NO",
                    WicketOSCore_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],                    
					GateCore_S:"NO",
                    GateCore_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],                    
                    СlearanceWicketOS_S:"",
                    СlearanceWicketOS_M:"___",
                    СlearanceWicketOS_P:"от 5мм до 150мм",
                    СlearanceWicketOS_C:{min:5, max:150},                    
					СrossbarWicketOS_S:"YES",
                    СrossbarWicketOS_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],                    
					TypeControlWicketOS_S:"6.7.1",
                    TypeControlWicketOS_Os:[{name:"Ручной",value:"6.7.1"},{name:"Автоматический",value:"6.7.2"}],                    
					LoopWicketOS_S:"6.6.1",
                    LoopWicketOS_Os:[{name:"Слева",value:"6.6.1"},{name:"Справа",value:"6.6.2"}],                    
                    TypeOpeningWicketOS_S:"6.5.1",
                    TypeOpeningWicketOS_Os:[{name:"Во двор",value:"6.5.1"},{name:"На улицу",value:"6.5.2"}],                    
                    ColorWicketOS_S:"6.4.1",
                    ColorWicketOS_Os:[{name:"В цвет рамы",value:"6.4.1"},{name:"8017 коричневый",value:"6.4.2"},{name:"3005 красное             вино",value:"6.4.3"},{name:"5005 синий",value:"6.4.4"},{name:"6005 зеленый мох",value:"6.4.5"},{name:"7004 серый",value:"6.4.6"}],                    
                    HeightWicketOS_S:"",
                    HeightWicketOS_M:"___",
                    HeightWicketOS_P:"от 800мм до 2500мм",
                    HeightWicketOS_C:{min:800, max:2500},                    
                    WidthWicketOS_S:"",
                    WidthWicketOS_M:"___",
                    WidthWicketOS_P:"от 750мм до 1250мм",
                    WidthWicketOS_C:{min:750, max:1200},                   
					WicketOS_S:"NO",
                    WicketOS_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],                    
                    DistanceWicketV_S:"",
                    DistanceWicketV_M:"___",
                    DistanceWicketV_C:"",
                    TypeOpeningWicketV_S:"5.3.1",
                    TypeOpeningWicketV_Os:[{name:"Во двор",value:"5.3.1"},{name:"На улицу",value:"5.3.2"}],                    
                    WidthWicketV_S:"",
                    WidthWicketV_M:"___",
                    WidthWicketV_P:"от 750мм до 1200мм",
                    WidthWicketV_C:{max:1200,min:750}, 
					WicketV_S:"NO",
                    WicketV_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],                    
					RemoteController_S:"NO",                    
					Antenna_S:"NO",                    
					LightWarning_S:"NO",
                    Photocells_S:"NO",
                    ModelElectricdrive_S:"NO",
                    ManufacturerAutomation_S:"NO",
                    ManufacturerAutomation_Os:[{name:"Нет (своя автоматика)",value:"NO"},{name:"An-Motors (Китай) ",value:"4.1.1"},{name:"Comunello (Италия) ",value:"4.1.2"},{name:"Came (Италия) ",value:"4.1.3"}],                    
                    //Bolt_S:"NO",
                    Bolt_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],                    
                    //Knob_S:"NO",
                    Knob_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],                    
                    RackGear_S:"NO",                    
                    SpacingProfile_S:"2.3.1",                    
                    ColorFilling_S:"2.2.1",
                    ColorFilling_Os:[{name:"В цвет рамы",value:"2.2.1"},{name:"8017 коричневый",value:"2.2.2"},{name:"3005 красное вино",value:"2.2.3"},{name:"5005 синий",value:"2.2.4"},{name:"6005 зеленый мох",value:"2.2.5"},{name:"7004 серый",value:"2.2.6"}],                    
                    MaterialSpecification_S:"2.1.1",//!!!
                    MaterialSpecificationAdd_S:"2.1.A",//!!!                    
                    AperturePilar_S:"NO",
                    AperturePilar_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],                    
                    SupportPillar_S:"NO",
                    SupportPillar_Os:[{name:"Нет",value:"NO"},{name:"Да",value:"YES"}],                    
                    TypeFilling_S:"1.9.1",
                    TypeFilling_Os:[{name:"Без заполнения",value:"1.9.1"},{name:"Профнастил",value:"1.9.2"},{name:"Штакетник",value:"1.9.3"},{name:"Сетка 3D",value:"1.9.4"},{name:"Решетка",value:"1.9.5"}],                    
                    DirectionOpening_S:"1.8.1",
                    DirectionOpening_Os:[{name:"Влево",value:"1.8.1"},{name:"Вправо",value:"1.8.2"}],                    
                    ColorFrame_S:"1.7.1",
                    ColorFrame_Os:[{name:"8017 коричневый",value:"1.7.1"},{name:"3005 красное вино",value:"1.7.2"},{name:"5005 синий",value:"1.7.3"},{name:"6005 зеленый мох",value:"1.7.4"},{name:"7004 серый",value:"1.7.5"}],                    
                    TypeRail_S:"1.6.1",                    
                    DistanceOpen_S:"",
                    DistanceOpen_M:"___",
                    DistanceOpen_C:"",                    
                    HeightOpening_S:"",
                    HeightOpening_M:"___",
                    HeightOpening_P:"от 1000мм до 2500мм",
                    HeightOpening_C:{max:2500,min:1000},                    
                    WidthOpening_S:"",
                    WidthOpening_M:"___",
                    WidthOpening_C:"",                    
                    TypeControl_S:"1.2.1",
                    TypeControl_Os:[{name:"Ручной",value:"1.2.1"},{name:"Автоматический",value:"1.2.2"}],                    
                /**
                *example result query DB
                *----\/----
                **/
                    TypeMainFrame_S: "<?=$a['default'];?>",
                    TypeMainFrame_Os: JSON.parse('<?=json_encode($a["data"]); ?>'),
                /**
                *----/\----
                **/   
                },
                computed:{
                    WidthOpening_P:function(){
                        let mes=0;
                        switch(this.TypeMainFrame_S) {
                            case '1.1.1': 
                            case '1.1.3':{
                                mes="от 2000мм до 5000мм";
                                this.WidthOpening_C={max:5000,min:2000};
                                break;
                            }
                            case '1.1.2': 
                            case '1.1.4':{
                                mes="от 2000мм до 8000мм";
                                this.WidthOpening_C={max:8000,min:2000};
                                break;
                            }
                            default: mes="Error";
                        }
                        return mes;
                    },
                    DistanceOpen_P:function(){//добавить обработка пустого и нулевого значений
                        let mes=0;
                        let _t=0;
                        if(this.WidthOpening_S>=2000 && this.WidthOpening_S<5000){
                            _t=Number(this.WidthOpening_S)+2000;
                            mes="от "+_t;
                            this.DistanceOpen_C={min:_t,max:100000};
                        }
                        if(this.WidthOpening_S>=5000 && this.WidthOpening_S<6500){
                            _t=Number(this.WidthOpening_S)+2500;
                            mes="от "+_t;
                            this.DistanceOpen_C={min:_t,max:100000};
                        }
                        if(this.WidthOpening_S>=6500 && this.WidthOpening_S<8000){
                            _t=Number(this.WidthOpening_S)+3000;
                            mes="от "+_t;
                            this.DistanceOpen_C={min:_t,max:100000};
                        }
                        return mes;
                    },
                    TypeRail_OBs:function(){return [{name:"ECO",value:"1.6.1"},{name:"EURO",value:"1.6.2"}]},
                    TypeRail_Os:function(){
                        let OBs=this.TypeRail_OBs;
                        if(Number(this.WidthOpening_S>5000)){                             
                            let mes=[];                            
                            this.TypeRail_S="1.6.2";                            
                            for(i=0; i<OBs.length; i++){
                                if(OBs[i].value=="1.6.2")
                                    mes.push(OBs[i]);
                            }                            
                            return mes;
                        }else{                            
//                            this.TypeRail_S="1.6.1";
                            return OBs;
                        }
                    },                  
                    MaterialSpecification_OBs:function(){return [{name:"Профнастил С8",value:"2.1.1"},{name:"Профнастил НС13",value:"2.1.2"},{name:"Штакетник П-образный",value:"2.1.3"},{name:"Штакетник M-образный",value:"2.1.4"},{name:"Стандарт 3мм",value:"2.1.5"},{name:"Силами клиента",value:"2.1.6"},{name:"Проф. труба 20*20*1,5",value:"2.1.7"},{name:"Проф. труба 20*40*1,5",value:"2.1.8"}]},
                    MaterialSpecification_Os:function(){
                        let OBs=this.MaterialSpecification_OBs;                                                        
                        let mes = [];
                        switch(this.TypeFilling_S) {
                            case '1.9.2':{
                                this.MaterialSpecification_S="2.1.1";
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="2.1.1" || OBs[i].value=="2.1.2")
                                        mes.push(OBs[i]);                       
                                }
                                break;
                            }
                            case '1.9.3':{
                                this.MaterialSpecification_S="2.1.3";
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="2.1.3" || OBs[i].value=="2.1.4")
                                        mes.push(OBs[i]); 
                                }
                                break;
                            }
                            case '1.9.4':{
                                this.MaterialSpecification_S="2.1.5";
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="2.1.5" || OBs[i].value=="2.1.6")
                                        mes.push(OBs[i]);                                          
                                }
                                break;
                            }
                            case '1.9.5':{
                                this.MaterialSpecification_S="2.1.7";
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="2.1.7" || OBs[i].value=="2.1.8")
                                        mes.push(OBs[i]);                           
                                }
                                break;
                            }
                            default: {
                                mes=[{name:"ERROR",val:"ERR"}];
                                this.MaterialSpecification_S="ERR"
                                break;
                            };
                        }
                        return mes;
                    },                    
                    MaterialSpecificationAdd_OBs:function(){return [{name:"c 1й стороны",value:"2.1.A"},{name:"c 2х сторон",value:"2.1.B"}]},
                    MaterialSpecificationAdd_Os:function(){
                        let OBs=this.MaterialSpecificationAdd_OBs;                                                        
                        let mes = [];
                        switch(this.TypeFilling_S) {
                            case '1.9.2':
                            case '1.9.3':{                                
                                this.MaterialSpecificationAdd_S="2.1.A";
                                mes=OBs;
                                break;
                            }
                            case '1.9.4':
                            case '1.9.5':{
                                this.MaterialSpecificationAdd_S="2.1.A";
                                mes.push(OBs[0]);
                                break;
                            }
                            default: {
                                mes=[{name:"ERROR",val:"ERR"}];
                                this.MaterialSpecificationAdd_S="ERR"
                                break;
                            };
                        }
                        return mes;
                    },                    
                    SpacingProfile_OBs:function(){return [{name:"50мм",value:"2.3.1"},{name:"100мм",value:"2.3.2"},{name:"120мм",value:"2.3.3"},{name:"150мм",value:"2.3.4"}]},                    
                    SpacingProfile_Os:function(){
                        let OBs=this.SpacingProfile_OBs;                                                        
                        let mes = [];
                        switch(this.TypeFilling_S) {
                            case '1.9.3':{
                                this.SpacingProfile_S="2.3.1";
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="2.3.1")
                                        mes.push(OBs[i]);                       
                                }
                                break;
                            }
                            case '1.9.5':{
                                this.SpacingProfile_S="2.3.2";
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value!="2.3.1")
                                        mes.push(OBs[i]);                           
                                }
                                break;
                            }
                            default: {
                                mes=[{name:"ERROR",val:"ERR"}];
                                this.SpacingProfile_S="ERR"
                                break;
                            };
                        }
                        return mes;
                    },
                    RackGear_OBs:function(){return [{name:"Нет",value:"NO"},{name:"Да",value:"YES"}]},
                    RackGear_Os:function(){
                        let mes=[];
                        let OBs=this.RackGear_OBs;
                        if(this.TypeControl_S=="1.2.1"){
                            mes=OBs;
//                            this.RackGear_S="NO";
                        }else{
                            mes.push(OBs[1]);
                            this.RackGear_S="YES";
                        }
                        return mes;
                    },
                    Knob_S:function(){
                        if(this.TypeControl_S=="1.2.1")
                            return "YES";
                        return "NO";
                            
                    },
                    Bolt_S:function(){
                        if(this.TypeControl_S=="1.2.1")
                            return "YES";
                        return "NO";
                    },
                    ModelElectricdrive_OBs:function(){return [{name:"Нет",value:"NO"},{name:"An-Motors ASL1000 (до 1000 кг.) ",value:"4.2.1"},{name:"An-Motors ASL2000 (до 2000 кг.) ",value:"4.2.2"},{name:"Comunello Fort 624 (до 600 кг.)",value:"4.2.3"},{name:"Comunello Fort 1000 (до 1000 кг.) ",value:"4.2.4"},{name:"Came BX-78 (до 800 кг.) ",value:"4.2.5"},{name:"Came BK-1200 (до 1200 кг.)",value:"4.2.6"}]},                        
                    ModelElectricdrive_Os:function(){
                        let OBs=this.ModelElectricdrive_OBs;                                                        
                        let mes = [{name:"Нет",value:"NO"}];
                        this.ModelElectricdrive_S="NO";
                        switch(this.ManufacturerAutomation_S) {
                            case '4.1.1':{
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="4.2.1" || OBs[i].value=="4.2.2")
                                        mes.push(OBs[i]);
                                }
                                break;
                            }
                            case '4.1.2':{
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="4.2.3" || OBs[i].value=="4.2.4")
                                        mes.push(OBs[i]);
                                }
                                break;
                            }
                            case '4.1.3':{
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="4.2.5" || OBs[i].value=="4.2.6")
                                        mes.push(OBs[i]);
                                }
                                break;
                            }
                            case 'NO':
                            default: {                                
                                break;
                            };
                        }
                        return mes;
                    },
                    Photocells_OBs:function(){return [{name:"Нет",value:"NO"},{name:"Фотоэлементы  P5103",value:"4.3.1"},{name:"Фотоэлементы  DTS",value:"4.3.2"},{name:"Фотоэлементы  / передатчик, приемник / накладные, дальность 10 м",value:"4.3.3"}]},
                    Photocells_Os:function(){                        
                        let OBs=this.Photocells_OBs;                                                        
                        let mes = [{name:"Нет",value:"NO"}];
                        this.Photocells_S="NO";
                        switch(this.ManufacturerAutomation_S) {
                            case '4.1.1':{
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="4.3.1")
                                        mes.push(OBs[i]);
                                }
                                break;
                            }
                            case '4.1.2':{
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="4.3.2")
                                        mes.push(OBs[i]);
                                }
                                break;
                            }
                            case '4.1.3':{
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="4.3.3")
                                        mes.push(OBs[i]);
                                }
                                break;
                            }
                            case 'NO':
                            default: {                                
                                break;
                            };
                        }
                        return mes;
                    }, 
                    LightWarning_OBs:function(){return [{name:"Нет",value:"NO"},{name:"Проблесковая лампа F5002",value:"4.4.1"},{name:"Лампа сигнальная SWIFT",value:"4.4.2"},{name:"Лампа сигнальная светодиодная 230 В",value:"4.4.3"}]},
                    LightWarning_Os:function(){                        
                        let OBs=this.LightWarning_OBs;                                                        
                        let mes = [{name:"Нет",value:"NO"}];
                        this.LightWarning_S="NO";
                        switch(this.ManufacturerAutomation_S) {
                            case '4.1.1':{
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="4.4.1")
                                        mes.push(OBs[i]);
                                }
                                break;
                            }
                            case '4.1.2':{
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="4.4.2")
                                        mes.push(OBs[i]);
                                }
                                break;
                            }
                            case '4.1.3':{
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="4.4.3")
                                        mes.push(OBs[i]);
                                }
                                break;
                            }
                            case 'NO':
                            default: {                                
                                break;
                            };
                        }
                        return mes;
                    },
                    Antenna_OBs:function(){return [{name:"Нет",value:"NO"},{name:"Антенна Частота 433,92 МГц ",value:"4.5.1"}]},
                    Antenna_Os:function(){                        
                        let OBs=this.Antenna_OBs;                                                        
                        let mes = [{name:"Нет",value:"NO"}];
                        this.Antenna_S="NO";
                        switch(this.ManufacturerAutomation_S) {
                            case '4.1.3':{
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="4.5.1")
                                        mes.push(OBs[i]);
                                }
                                break;
                            }
                            case 'NO':
                            default: {                                
                                break;
                            };
                        }
                        return mes;
                    },   
                    RemoteController_OBs:function(){return [{name:"Нет",value:"NO"},{name:"Пульт ДУ AT-4",value:"4.6.1"},{name:"Двухканальный пульт ДУ Keep 2 ",value:"4.6.2"},{name:"Четырехканальный пульт ДУ Keep 4 ",value:"4.6.3"},{name:"Брелок-передатчик 2-х канальный. Функция \"key code\" ",value:"4.6.4"},{name:"Брелок-передатчик 4-х канальный. Функция \"key code\" ",value:"4.6.5"}]},
                    RemoteController_Os:function(){                        
                        let OBs=this.RemoteController_OBs;                                                        
                        let mes = [{name:"Нет",value:"NO"}];
                        this.RemoteController_S="NO";
                        switch(this.ManufacturerAutomation_S) {
                            case '4.1.1':{
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="4.6.1")
                                        mes.push(OBs[i]);
                                }
                                break;
                            }
                            case '4.1.2':{
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="4.6.2" || OBs[i].value=="4.6.3")
                                        mes.push(OBs[i]);
                                }
                                break;
                            }
                            case '4.1.3':{
                                for(i=0; i<OBs.length; i++){
                                    if(OBs[i].value=="4.6.4" || OBs[i].value=="4.6.5")
                                        mes.push(OBs[i]);
                                }
                                break;
                            }
                            case 'NO':
                            default: {                                
                                break;
                            };
                        }
                        return mes;
                    }, 
                    DistanceWicketV_P:function(){
                        let mes=0;
                        let max = (Number(this.WidthOpening_S)/2)-(Number(this.WidthWicketV_S)/2);
                        mes = "от 300мм до "+max+"мм";
                        this.DistanceWicketV_C={min:300,max:max};
                        return mes;
                    },
                    GateBasisAdd_OBs:function(){return [{name:"Стандарт",value:"NO"},{name:"С перемычкой",value:"8.1.A"},{name:"Усиленное",value:"8.1.B"}]},
                    GateBasisAdd_Os:function(){
                        let OBs=this.GateBasisAdd_OBs;                                                        
                        let mes = [];
                        switch(this.GateBasis_S) {
                            case '8.1.1':{
                                this.GateBasisAdd_hs=true;
                                this.GateBasisAdd_S="NO";
                                mes.push(OBs[0]);
                                mes.push(OBs[1]);
                                break;
                            }
                            case '8.1.2':{
                                this.GateBasisAdd_hs=true;
                                this.GateBasisAdd_S="NO";
                                mes.push(OBs[0]);
                                mes.push(OBs[2]);
                                break;
                            }
                            case 'NO':{
                                this.GateBasisAdd_hs=false;
                                this.GateBasisAdd_S="NO";
                                mes=[{name:"Стандарт",value:"NO"}];
                                break;
                            }
                            default: {
                                mes=[{name:"ERROR",val:"ERR"}];
                                this.MaterialSpecificationAdd_S="ERR"
                                break;
                            };
                        }
                        return mes;
                    },
                    GateBasisAdd_hs:true,
                    SettingsFilling:function(){return this.ShowOrHideLine(this.TypeFilling_S=="1.9.1")},
                    ColorFilling:function(){return this.ShowOrHideLine(this.TypeFilling_S=="1.9.4" || this.TypeFilling_S=="1.9.5")},
                    SpacingProfile:function(){return this.ShowOrHideLine(this.TypeFilling_S=="1.9.4" || this.TypeFilling_S=="1.9.5")},
                    Automation:function(){return this.ShowOrHideLine(this.TypeControl_S=="1.2.1")},
                    WidthWicketV:function(){return this.ShowOrHideLine(this.WicketV_S=="NO")},
                    TypeOpeningWicketV:function(){return this.ShowOrHideLine(this.WicketV_S=="NO")},
                    DistanceWicketV:function(){return this.ShowOrHideLine(this.WicketV_S=="NO")},                    
                    WidthWicketOS:function(){return this.ShowOrHideLine(this.WicketOS_S=="NO")},
                    HeightWicketOS:function(){return this.ShowOrHideLine(this.WicketOS_S=="NO")},
                    ColorWicketOS:function(){return this.ShowOrHideLine(this.WicketOS_S=="NO")},
                    TypeOpeningWicketOS:function(){return this.ShowOrHideLine(this.WicketOS_S=="NO")},
                    LoopWicketOS:function(){return this.ShowOrHideLine(this.WicketOS_S=="NO")},
                    TypeControlWicketOS:function(){return this.ShowOrHideLine(this.WicketOS_S=="NO")},
                    СrossbarWicketOS:function(){return this.ShowOrHideLine(this.WicketOS_S=="NO")},
                    СlearanceWicketOS:function(){return this.ShowOrHideLine(this.WicketOS_S=="NO")},
                    MetalPilarMounting:function(){return this.ShowOrHideLine(this.WicketOSMetalPilar_S=="NO")},
                    Garbage:function(){return this.ShowOrHideLine(this.FillingMounting_S=="NO" && this.LightWarningMounting_S=="NO" && this.PhotocellsMounting_S=="NO" && this.ElectricdriveMounting_S=="NO" && this.WicketOSMounting_S=="NO" && this.GateMounting_S=="NO" && this.MetalPilarMounting=="NO")},                    
                    Rent:function(){return this.ShowOrHideLine(this.RentTool_S=="NO")},
                    DeliveryDistance:function(){return this.ShowOrHideLine(this.Delivery_S=="NO")}
                },
                methods:{
                    noChar: function(e, val){
                        if(e.key.length === 1){let pattern = new RegExp("^[0-9]$");
                            let key = e.key;//1
                            let mode_key = e.altKey || e.ctrlKey; //false
                            if(!pattern.test(key) && !mode_key){
                                e.preventDefault();
                                return false;
                            }else{
                                if(Number(key)===0 && Number(val)===0 || val.length>10){
                                    e.preventDefault();
                                    return false;
                                }
                            }
                        }
                    },
                    messageBox: function(a, option){
                        let mes = "___";
                        let max = eval("this."+a+"_C.max");
                        let min = eval("this."+a+"_C.min");
                        let val = Number(eval("this."+a+"_S"));
                        eval("this."+a+"_S='"+(val==0?'':val)+"'");
                        if(val !== 0){
                            if(option === 'keyup'){
                                if(val>max)
                                    mes = "Указанное значение больше допустимого";
                            }
                            if(option === 'blur'){
                                if(val<min)
                                    mes = "Указанное значение меньше допустимого";
                                if(val>max)
                                    mes = "Указанное значение больше допустимого";
                            }
                        }else{
                            message = "___";
                        }
                        eval("this."+a+"_M='"+mes+"'");
                    },
                    ShowOrHideLine: function(condition){
                        if(condition)
                            return false;
                        else
                            return true;                        
                    }
                },
                watch:{ }
            });    
            function getCaretPosition(obj){
                let cursorPos = null;
                if (document.selection){
                    var range = document.selection.createRange();
                    range.moveStart('textedit', -1);
                    cursorPos = range.text.length;
                }else{
                    cursorPos = obj.selectionStart;
                }
                return cursorPos;
            }        
    </script>
</body>
</html>