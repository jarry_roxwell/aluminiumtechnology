<?php

	namespace AlTech\router;

	class Router{			
		public function __construct(){			
			$this->_routes = include(ROOT.'/router/route.php');			
		}
		private function getLaw(){
            if(empty($_GET['route']))
		        $this_rout="";
		    else
                $this_rout = trim($_GET['route'], '/');
            foreach($this -> _routes as $mask => $law){
                if(preg_match("~".$mask."~i", $this_rout)){
                    if($law['value']){
                        unset($_GET['route']);                        
                        $law['get'] = $_GET;
                    }                    
                    return $law;
                }
            }
            return 0;
        }
		public function run() {            
            $law = $this->getLaw();
			$path = ROOT.'/controlers/'.$law['controler'].'Control.php';			
			if(file_exists($path))
				include_once($path);				
			else
				return false;
			$controler = 'AlTech\controlers\\'.$law['controler'].'Control';			
			$action = 'action'.$law['action'];
			$controler = new $controler;
			$answer=$controler -> $action($law['get']);
            return $answer;
		}
		private $_routes;
	}