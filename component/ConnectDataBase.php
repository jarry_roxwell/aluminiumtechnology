<?php

	namespace alTech\ConnectDataBase;

	class ConnectDataBase extends \PDO {
		function __construct($control){
			if(!$control) { 
				header('Location: ../');
				exit(); 
			}
			try {
				parent::__construct('mysql:host=*host*; dbname=*dbname*', '*name*', '*pass*');
				$this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				$this->setAttribute(\PDO::ATTR_ORACLE_NULLS, \PDO::NULL_TO_STRING);
				$this->exec('SET NAMES "utf8"');
			} catch(\PDOException $e) {
				header('Location: ../'); 
				exit();
			}
		}
	}