<?php

	namespace alTech\models\CalculateModel;

	use alTech\component\ConnectDataBase;

	include(ROOT.'/component/ConnectDataBase.php');
	
	class CalculateModel {
		function __construct() {
			try {
				$this->_db = new ConnectDataBase(true);
				if(!$this->_db) 
					return 0;
			} catch(PDOException $e) {
				return 0;
			}
		}
		private $_db=0;
	}