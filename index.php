<?php
    
	use AlTech\router\Router;

	define('ROOT', dirname(__FILE__));
	
	include_once(ROOT.'/router/router.php');
	
	ob_start();
	$router = new Router();
	$answer = $router->run();
	if($answer) {
		ob_end_flush();
	} else {
		ob_clean();
		include_once(ROOT.'/dist/html/error.html');
		ob_end_flush();
	}